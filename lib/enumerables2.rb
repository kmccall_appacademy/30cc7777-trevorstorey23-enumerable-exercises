require 'byebug'

# EASY

# Define a method that returns the sum of all the elements in its argument (an
# array of numbers).
def array_sum(arr)
  arr.reduce(:+) || 0
end

# Define a method that returns a boolean indicating whether substring is a
# substring of each string in the long_strings array.
# Hint: you may want a sub_tring? helper method
def in_all_strings?(long_strings, substring)
  long_strings.all? { |word| word.include?(substring) }
end

# Define a method that accepts a string of lower case words (no punctuation) and
# returns an array of letters that occur more than once, sorted alphabetically.
def non_unique_letters(string)
  no_ws_arr = string.delete(" ").split("")
  non_uniq_chars = no_ws_arr.select { |chr| no_ws_arr.count(chr) > 1  }
  non_uniq_chars.uniq
end

# Define a method that returns an array of the longest two words (in order) in
# the method's argument. Ignore punctuation!
def longest_two_words(string)
  sorted_by_word_length = string.split(" ").sort_by { |word| word.length }
  [sorted_by_word_length[-1], sorted_by_word_length[-2]]
end

# MEDIUM

# Define a method that takes a string of lower-case letters and returns an array
# of all the letters that do not occur in the method's argument.
def missing_letters(string)
  ("a".."z").reject { |char| string.include?(char) }
end

# Define a method that accepts two years and returns an array of the years
# within that range (inclusive) that have no repeated digits. Hint: helper
# method?
def no_repeat_years(first_yr, last_yr)
  (first_yr..last_yr).select { |year| not_repeat_year?(year) }
end

def not_repeat_year?(year)
  year.to_s.split("").uniq == year.to_s.split("")
end

# HARD

# Define a method that, given an array of songs at the top of the charts,
# returns the songs that only stayed on the chart for a week at a time. Each
# element corresponds to a song at the top of the charts for one particular
# week. Songs CAN reappear on the chart, but if they don't appear in consecutive
# weeks, they're "one-week wonders." Suggested strategy: find the songs that
# appear multiple times in a row and remove them. You may wish to write a helper
# method no_repeats?

# find all the songs that do have consecutive repeats and put them in an array.
  # iterate. if the current element is the same as the last element
  # push it into array.
# reject all occurances of those songs in the original array
# return a uniq version of the result of that array.
def one_week_wonders(songs)
  not_wonder_songs = songs.select.with_index do |song, i|
    next if i == 0
    song == songs[i-1]
  end
  
  wonder_songs = songs.reject { |song| not_wonder_songs.include?(song) }
  wonder_songs.uniq
end

# Define a method that, given a string of words, returns the word that has the
# letter "c" closest to the end of it. If there's a tie, return the earlier
# word. Ignore punctuation. If there's no "c", return an empty string. You may
# wish to write the helper methods c_distance and remove_punctuation.

def for_cs_sake(string)
  no_punc_words = string.gsub(/[[:punct:]]/, '').split(" ")
  c_words = no_punc_words.select { |word| word.include?("c")}
  c_words_distance = c_words.map { |word| c_distance(word) }
  c_words.min
end

def c_distance(word)
  idx_of_c = 99999

  i = word.length - 1
  while i >= 0
    if word[i] == "c"
      idx_of_c = i
      break
    end
    i -= 1
  end

  (word.length - 1) - idx_of_c
end

# Define a method that, given an array of numbers, returns a nested array of
# two-element arrays that each contain the start and end indices of whenever a
# number appears multiple times in a row. repeated_number_ranges([1, 1, 2]) =>
# [[0, 1]] repeated_number_ranges([1, 2, 3, 3, 4, 4, 4]) => [[2, 3], [4, 6]]

def repeated_number_ranges(arr)
  result_arr = []

  i = 1
  while i < arr.length
    num = arr[i]

    if num == arr[i-1]
      start_range_idx = i-1
      end_range_idx = i

      i += 1
      while arr[i] == num && i < arr.length
        end_range_idx += 1
        i += 1
      end
      result_arr << [start_range_idx, end_range_idx]

    end

    i += 1
  end

  result_arr
end
